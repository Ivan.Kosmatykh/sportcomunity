<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Sign in</title>

      <link href="resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="resources/css/common.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
        <h2 class="hello-title">${userName}</h2>

        <div class="w-25 p-3">
            <div clss="w-10 d-inline">Sports</div>
            <div>
                <c:if test="${not empty sports}">
                    <ul>
                        <c:forEach var="sport" items="${sports}">
                            <a href="/user/sport/${sport}">${sport}</a>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
  </body>
</html>
