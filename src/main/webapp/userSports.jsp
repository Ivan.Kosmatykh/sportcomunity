<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Register your profile</title>

      <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="/resources/css/common.css" rel="stylesheet">
      <link href="/resources/css/bootstrap-tagsinput.css" rel="stylesheet" >
  </head>

  <body>

    <div class="container">
        <h2 class="form-heading">Your sports</h2>
        <form method="POST" class="form">
            <textarea id="sports" class="text" name="sports">${sports}</textarea>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
        </form>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>
