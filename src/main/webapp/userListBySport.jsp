<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Sign in</title>

      <link href="resources/css/bootstrap.min.css" rel="stylesheet">
      <link href="resources/css/common.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
        <h2 class="hello-title">Users with sport <b>${sportName}</b></h2>

        <div class="w-25 p-3">
            <div>
                <c:if test="${not empty users}">
                    <ul>
                        <c:forEach var="user" items="${users}">
                            <a href="/user/${user.getId()}/profile">${user.getUserName()}</a>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
  </body>
</html>
