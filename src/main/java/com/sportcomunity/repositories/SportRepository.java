package com.sportcomunity.repositories;

import com.sportcomunity.models.Sport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface SportRepository extends JpaRepository<Sport, Long> {
    Set<Sport> findByName(String sportName);
}
