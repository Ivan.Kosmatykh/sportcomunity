package com.sportcomunity.controllers.team;

import com.sportcomunity.models.Sport;
import com.sportcomunity.models.TeamInvite;
import com.sportcomunity.models.User;
import com.sportcomunity.services.security.SecurityService;
import com.sportcomunity.services.team.TeamService;
import com.sportcomunity.services.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;


@Controller
public class TeamController {
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserService userService;
    @Autowired
    private TeamService teamService;

    @PostMapping("/user/team")
    public String createTeam(
            @NonNull @PathVariable(value="userId") Long userId,
            @NonNull @RequestParam("teamName") String teamName,
            @NonNull @RequestParam("sportName") String sportName) {

        User currentUser = userService.findByUserName(securityService.getPrincipalName());
        teamService.createTeam(currentUser, teamName, sportName);

        return "redirect:/profile";
    }

    @PostMapping("/user/{userId}/team/{teamId}/invitation")
    public String inviteUser(
            @NonNull @PathVariable(value="userId") Long userId,
            @NonNull @PathVariable(value="teamId") Long teamId) {

        User author = userService.findByUserName(securityService.getPrincipalName());
        teamService.inviteUser(author, userId, teamId);
        return "redirect:/profile";
    }

    @PostMapping("/user/invitation/{invitationId}/")
    public String resolveInvitation(
            @NonNull @PathVariable(value="userId") Long userId,
            @NonNull @PathVariable(value="teamId") Long invitationId,
            @NonNull @RequestParam("statusName") String status,
            BindingResult bindingResult) {

        User user = userService.findByUserName(securityService.getPrincipalName());
        if (status.equals(TeamInvite.ACCEPTED)) {
            teamService.acceptInvite(invitationId, user);
        } else if (status.equals(TeamInvite.REJECTED)) {
            teamService.rejectInvite(invitationId, user);
        } else {
            bindingResult.rejectValue("statusName", "User.invitation.statusName");
            return "userInvitationCreation";
        }

        return "redirect:/profile";
    }
}
