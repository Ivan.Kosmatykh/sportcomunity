package com.sportcomunity.controllers.profile;

import com.sportcomunity.models.Sport;
import com.sportcomunity.models.User;
import com.sportcomunity.services.security.SecurityService;
import com.sportcomunity.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;


@Controller
public class UserProfileController {
    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserService userService;

    @GetMapping("/profile")
    public String getProfile(@NonNull Model model) {
        User user = userService.findByUserName(securityService.getPrincipalName());
        model.addAttribute("userName", user.getUserName());
        model.addAttribute("userId", user.getId());

        List<String> sports = new LinkedList<String>();
        for(Sport userSport: user.getSports()) {
            sports.add(userSport.getName());
        }

        model.addAttribute("sports", String.join(",", sports));

        return "profile";
    }

    @GetMapping("/user/{userId}/sports")
    public String getUserProfileSports(@NonNull Model model, @PathVariable(value="userId") Long userId) {
        User user = userService.findByUserName(securityService.getPrincipalName());
        if (!user.getId().equals(userId)) {
            throw new org.springframework.security.access.AccessDeniedException("403 returned");
        }

        List<String> sports = new LinkedList<String>();
        for(Sport userSport: user.getSports()) {
            sports.add(userSport.getName());
        }

        model.addAttribute("sports", String.join(",", sports));

        return "userSports";
    }

    @PostMapping("/user/{userId}/sports")
    public String setUserProfileSports(
            @PathVariable(value="userId") Long userId,
            @NonNull @RequestParam("sports") String sports) {

        User user = userService.findByUserName(securityService.getPrincipalName());
        if (!user.getId().equals(userId)) {
            throw new org.springframework.security.access.AccessDeniedException("403 returned");
        }
        Set<String> sportsSet = new HashSet<>(
                Arrays.asList(sports.split(",")))
                .stream().map(String::trim)
                .collect(Collectors.toSet());

        userService.setSports(user, sportsSet);
        return "redirect:/profile";
    }

    @GetMapping("/user/sport/{sportName}")
    public String findUsersBySports(@NonNull Model model, @NonNull @PathVariable(value="sportName") String sportName) {
        Set<User> users = userService.findUsersBySportName(sportName);
        model.addAttribute("users", users);
        model.addAttribute("sportName", sportName);
        return "userListBySport";
    }

    @GetMapping("/user/{userId}/profile")
    public String getUserProfileById(@NonNull Model model, @PathVariable(value="userId") Long userId) {
        User currentUser = userService.findByUserName(securityService.getPrincipalName());
        if (currentUser.getId().equals(userId)) {
            return "redirect:/profile";
        }

        User user = userService.getUserById(userId);

        List<String> sports = new LinkedList<String>();
        for(Sport userSport: user.getSports()) {
            sports.add(userSport.getName());
        }

        model.addAttribute("sports", String.join(",", sports));

        return "anotherUserProfile";
    }
}
