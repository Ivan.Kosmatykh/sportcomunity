package com.sportcomunity.models;


import javax.persistence.*;


@Entity
@Table(name = "team_member")
public class TeamMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public TeamMember setName(String name) {
        this.name = name;
        return this;
    }

    public Team getTeam() {
        return team;
    }

    public TeamMember setTeam(Team team) {
        this.team = team;
        return this;
    }

    public User getUser() {
        return user;
    }

    public TeamMember setUser(User user) {
        this.user = user;
        return this;
    }
}
