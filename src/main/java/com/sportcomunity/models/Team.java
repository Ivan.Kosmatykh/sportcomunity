package com.sportcomunity.models;

import javax.persistence.*;


@Entity
@Table(name = "team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String sportName;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Team setName(String name) {
        this.name = name;
        return this;
    }

    public String getSportName() {
        return sportName;
    }

    public Team setSportName(String sportName) {
        this.sportName = sportName;
        return this;
    }

    public User getOwner() {
        return owner;
    }

    public Team setOwner(User owner) {
        this.owner = owner;
        return this;
    }
}
