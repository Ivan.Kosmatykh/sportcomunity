package com.sportcomunity.models;


import javax.persistence.*;


@Entity
@Table(name = "team_invite")
public class TeamInvite {
    public static final String INVITED = "invited";
    public static final String ACCEPTED = "accepted";
    public static final String REJECTED = "rejected";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String status;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public TeamInvite setStatus(String status) {
        if (!(
                status.equals(TeamInvite.INVITED)
                        || status.equals(TeamInvite.ACCEPTED)
                        || status.equals(TeamInvite.REJECTED))) {
            throw new RuntimeException("Invalid invitation status");
        }
        this.status = status;
        return this;
    }

    public Team getTeam() {
        return team;
    }

    public TeamInvite setTeam(Team team) {
        this.team = team;
        return this;
    }

    public User getUser() {
        return user;
    }

    public TeamInvite setUser(User user) {
        this.user = user;
        return this;
    }

    public User getAuthor() {
        return author;
    }

    public TeamInvite setAuthor(User author) {
        this.author = author;
        return this;
    }
}
