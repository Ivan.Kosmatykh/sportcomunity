package com.sportcomunity.services.security;

import com.sportcomunity.models.User;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);

    String getPrincipalName();
}
