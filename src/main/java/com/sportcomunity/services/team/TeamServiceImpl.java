package com.sportcomunity.services.team;


import com.sportcomunity.models.User;
import com.sportcomunity.models.Team;
import com.sportcomunity.models.TeamMember;
import com.sportcomunity.models.TeamInvite;

import com.sportcomunity.repositories.UserRepository;

import com.sportcomunity.repositories.TeamRepository;
import com.sportcomunity.repositories.TeamInviteRepository;
import com.sportcomunity.repositories.TeamMemberRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamMemberRepository teamMemberRepository;
    @Autowired
    private TeamInviteRepository teamInviteRepository;
    @Autowired
    private UserRepository userRepository;

    public TeamInvite inviteUser(User author, Long userId, Long teamId) {
        User user = userRepository.getOne(userId);
        Team team = teamRepository.getOne(teamId);

        TeamInvite teamInvite = new TeamInvite();
        teamInviteRepository.save(
                teamInvite
                        .setAuthor(author)
                        .setUser(user)
                        .setTeam(team)
                        .setStatus(TeamInvite.INVITED)
        );
        return teamInvite;
    }

    public void acceptInvite(Long inviteId, User user) {
        resolveInvite(inviteId, user, TeamInvite.ACCEPTED);
    }

    public void rejectInvite(Long inviteId, User user) {
        resolveInvite(inviteId, user, TeamInvite.REJECTED);
    }

    public Team createTeam(User user, String teamName, String sportName) {
        Team team = new Team();
        teamRepository.save(team.setName(teamName).setSportName(sportName).setOwner(user));
        teamMemberRepository.save(
                new TeamMember()
                        .setName(user.getUserName())
                        .setUser(user)
                        .setTeam(team)
        );
        return team;
    }

    private void resolveInvite(Long inviteId, User user, String status) {
        TeamInvite invite = teamInviteRepository.getOne(inviteId);

        if (!invite.getUser().getId().equals(user.getId())) {
            throw new RuntimeException(String.format("Could not apply %s on not own invitation", status));
        }

        Team team = invite.getTeam();

        invite.setStatus(status);
        teamInviteRepository.save(invite);

        if (status.equals(TeamInvite.ACCEPTED)) {
            teamMemberRepository.save(
                    new TeamMember()
                            .setName(user.getUserName())
                            .setUser(user)
                            .setTeam(team)
            );
        }
    }
}
