package com.sportcomunity.services.team;

import com.sportcomunity.models.Team;
import com.sportcomunity.models.TeamInvite;
import com.sportcomunity.models.User;


public interface TeamService {
    TeamInvite inviteUser(User author, Long userId, Long teamId);
    void acceptInvite(Long inviteId, User user);
    void rejectInvite(Long inviteId, User user);
    Team createTeam(User user, String teamName, String sportName);
}
