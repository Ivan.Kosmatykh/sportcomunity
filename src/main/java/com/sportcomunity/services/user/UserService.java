package com.sportcomunity.services.user;

import com.sportcomunity.models.User;

import java.util.Set;

public interface UserService {
    void create(User user);

    User findByUserName(String username);

    String encodePassword(String password);

    Boolean checkPassword(User user, String password);

    void setSports(User user, Set<String> sportNames);

    Set<User> findUsersBySportName(String sportname);

    User getUserById(Long userId);
}
