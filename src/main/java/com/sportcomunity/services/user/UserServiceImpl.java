package com.sportcomunity.services.user;


import com.sportcomunity.models.Sport;
import com.sportcomunity.models.User;
import com.sportcomunity.repositories.RoleRepository;
import com.sportcomunity.repositories.SportRepository;
import com.sportcomunity.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private SportRepository sportRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void create(User user) {
        user.setPassword(encodePassword(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public String encodePassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    @Override
    public Boolean checkPassword(User user, String password) {
        return bCryptPasswordEncoder.matches(password, user.getPassword());
    }

    @Override
    public User findByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public void setSports(User user, Set<String> sportNames){
        sportRepository.deleteInBatch(user.getSports());

        Set<Sport> newUserSports = new HashSet<Sport>();
        for (String sportName: sportNames) {
            newUserSports.add(
                    new Sport()
                            .setName(sportName)
                            .setUser(user)
            );
        }

        sportRepository.saveAll(newUserSports);

        user.setSports(newUserSports);
        userRepository.save(user);
    }

    @Override
    public Set<User> findUsersBySportName(String sportName) {
        return sportRepository
                .findByName(sportName)
                .stream()
                .map(Sport::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.getOne(userId);
    }
}
